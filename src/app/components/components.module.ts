import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PeliculaComponent } from './pelicula/pelicula.component';
import { PipesModule } from '../pipes/pipes.module';



@NgModule({
  declarations: [ PeliculaComponent ],
  imports: [
    CommonModule,
    PipesModule
  ],
  exports: [
    PeliculaComponent
  ]
})
export class ComponentsModule { }
