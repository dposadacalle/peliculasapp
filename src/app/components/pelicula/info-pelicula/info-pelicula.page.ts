import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-info-pelicula',
  templateUrl: './info-pelicula.page.html',
  styleUrls: ['./info-pelicula.page.scss'],
})
export class InfoPeliculaPage implements OnInit {

  @Input() original_title: string;
  @Input() overview: string;
  @Input() popularity: string;
  @Input() title: string;

  constructor() { }

  ngOnInit() {
    console.log(this.original_title);
  }

}
