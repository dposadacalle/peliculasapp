import { Component } from '@angular/core';
import { Pelicula, Respuesta } from '../core/interface';
import { MovieDBService } from '../services/movie-db.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  lastMovies: Pelicula[] = [];

  constructor(private _movieService: MovieDBService) {
    this.getMovies();
  }

  public getMovies(){
    
    this._movieService.getAllMovies()
        .subscribe( data => {

          console.log(data.results);

          this.lastMovies = data.results;

        });

  }

}
