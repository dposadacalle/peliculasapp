import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Respuesta } from '../core/interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MovieDBService {

  constructor(private _httpClient: HttpClient) { }

  public getAllMovies(): Observable<Respuesta>{

    return this._httpClient.get<Respuesta>(`https://api.themoviedb.org/3/discover/movie?api_key=14ec01deb203c7fdbe4c8e2bd502d848&primary_release_date.gte=2014-09-15&primary_release_date.lte=2014-10-22&language=es&include_image_language=es`);

  }
  
}
