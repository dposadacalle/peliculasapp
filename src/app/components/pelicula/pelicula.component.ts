import { Component, Input, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InfoPeliculaPage } from './info-pelicula/info-pelicula.page';

@Component({
  selector: 'app-pelicula',
  templateUrl: './pelicula.component.html',
  styleUrls: ['./pelicula.component.scss'],
})
export class PeliculaComponent implements OnInit {

  @Input() pelicula: any; 

  constructor(public modalController: ModalController) {}

  ngOnInit() {
    console.log(this.pelicula);
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: InfoPeliculaPage,
      cssClass: 'my-custom-class',
      componentProps: {
        'original_title': this.pelicula.original_title,
        'overview': this.pelicula.overview,
        'popularity': this.pelicula.popularity,
        'title': this.pelicula.title
      }
    });
    return await modal.present();
  }

}
